package com.graphaware.pizzeria.service;

import com.graphaware.pizzeria.model.Pizza;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AmountCalculator {

    public static final String PINEAPPLE = "pineapple";

    public Double calculateAmount(List<Pizza> pizzas) {
        double totalPrice = 0;
        if (pizzas == null) {
            return 0.0;
        }
        // buy a pineapple pizza, get 10% off the others
        boolean applyPineappleDiscount = false;
        for (Pizza pizza : pizzas) {
            if (pizza.getToppings().contains(PINEAPPLE)) {
                applyPineappleDiscount = true;
            }
        }
        double cheapestPrice = pizzas.get(0).getPrice();
        for (Pizza pizza : pizzas) {
            if (pizza.getToppings().contains(PINEAPPLE)) {
                totalPrice += pizza.getPrice();
                if (pizza.getPrice() < cheapestPrice) {
                    cheapestPrice = pizza.getPrice();
                }
            } else {
                if (applyPineappleDiscount) {
                    totalPrice += pizza.getPrice() * 0.9;
                    if (pizza.getPrice() < cheapestPrice) {
                        cheapestPrice = pizza.getPrice() * 0.9;
                    }
                } else {
                    totalPrice += pizza.getPrice();
                    if (pizza.getPrice() < cheapestPrice) {
                        cheapestPrice = pizza.getPrice();
                    }
                }
            }
        }

        if (pizzas.size() >= 3) {
            totalPrice -= cheapestPrice;
        }
        return totalPrice;
    }

}
