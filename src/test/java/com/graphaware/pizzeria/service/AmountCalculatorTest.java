package com.graphaware.pizzeria.service;

import com.graphaware.pizzeria.model.Pizza;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.graphaware.pizzeria.service.AmountCalculator.PINEAPPLE;
import static org.junit.jupiter.api.Assertions.*;

class AmountCalculatorTest {

    AmountCalculator amountCalculator = new AmountCalculator();

    @Test
    public void get10PercentDiscountOnOtherPizzasInCaseOfPineapple() {
        double amount = amountCalculator.calculateAmount(Arrays.asList(pineapplePizza(), margarita()));

        assertEquals(14.5, amount);
    }

    @Test
    public void cheapestPizzaIsFreeIf3OrMorePizzas() {
        assertEquals(23.5, amountCalculator.calculateAmount(Arrays.asList(pineapplePizza(), margarita(), fruttiDiMare())));
        assertEquals(20, amountCalculator.calculateAmount(Arrays.asList(margarita(), margarita(), fruttiDiMare())));
    }

    private Pizza pineapplePizza() {
        Pizza pizza = new Pizza();
        pizza.setToppings(Arrays.asList(PINEAPPLE, "cheese", "Parmigiano"));
        pizza.setPrice(10d);
        return pizza;
    }

    private Pizza margarita() {
        Pizza pizza = new Pizza();
        pizza.setToppings(Arrays.asList("cheese", "ketchup"));
        pizza.setPrice(5d);
        return pizza;
    }

    private Pizza fruttiDiMare() {
        Pizza pizza = new Pizza();
        pizza.setToppings(Arrays.asList("scampi", "mussels"));
        pizza.setPrice(15d);
        return pizza;
    }

}