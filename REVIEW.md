# Part 1

!!NOTE: IMHO Section 2,3,4 are a little overlapping in some topics

## 1. What do you think about the overall architecture of this app?

The application implement a webservice for managing pizzas and orders. The overall architecture is a standard in general.
We have a controller layer, a service layer, and a data access layer separated (basically JPA + spring data). Where we have a separate data model.
(Not always sometimes there is direct controller to data layer, I consider this a bas practice. See pizza controller.)
Regarding of domain modelling, this is called anemic domain model, where we separate the logic
(service layer) from the data (data model + data access layer) layer . 
Note that in this case the controller layer serves as the adapter if we look at this as a 
[Hexagonal architecture](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software))

This architecture is usually used in spring based applications, there are no fundamental problems with this setup.
(However would consider to use feature packages, instead of this layering)

There are more issues with the Webservice API design. It could be described as HTTP API (what is a very broad term), while it would fit into paradigms
of RESTful API. Typically, Pizzas and Purchases could be understood as resources, so using only CRUD operations we 
could simplify the API from a client perspective. (Also an Open API description would be nice)
 
There are more issues with this API, typically `purchaseService.addPizzaToPurchase` has some 
concurrency issues. Note that the API has no support for optimistic locking (or optimistic concurrency control). 
So calling this service concurrently (or on a multi-instance setup) could lead to a "Lost update problem."
(Note this could be done adding it into the API like a version fields, or just as HTTP already defines it with E-TAG and if-match headers.)
API also has a strange addressability approach.

Similar is true also for database access - JPA supports @Version annotation because of similar reasons. Won't elaborate this
here in detail, but the point in short is that it will make transaction serializable without actually using that level of isolation
in the DB. (Typically on entities which change like Purchase)  

The whole Domain model is also strange, would prefer to have predefined pizzas, that can be associated with a purchase, with optional additional toppings.
(At least this is how works in Pizzerias here :) )    
   
   
## 2. What would be the most valuable improvements ? (prioritized)

- Fixing the optimistic locking issues. (Basically the application is not correct in terms of concurrency)
- Have a nice REST API, with open API specs. (Maybe even HATEOAS - although personally not fan of it.)
- Complete unit testing (unit, integration and functional tests (in this case contract tests)) 
- Don't use DB as the user realm, use JWT (OAuth2) tokens. Use some authn/authz service like Auth0 or AWS Cognito. 
- Clearly separate always a service layer between the controller and data access layer. 
 
## 3. What’s missing to have the application production ready?

(Note: Spring boot does a pretty good job in this by default, like http content compression (gzip), Hikari datasource etc)

- Application lacks logging, so using a logging framework (log4j or logback) through SLF4J would be nice (replace all printlines). 
   (There are more strategies to this, what and when to log). Use json logging format for to support aggregation (think ELK).
- Use @Version annotation on JPA Entities.
- Liquibase or Flyway for DB management. Without that DB changes are much more painful.
- Use Spring Boot actuators - essential for health endpoint for Kubernetes and other container services
- Micrometer: https://spring.io/blog/2018/03/16/micrometer-spring-boot-2-s-new-application-metrics-collector  for metrics
- I would use DTO and JPA Entities everywhere to separate the controller layer from service layer. (Bit conservative of this because of detached JPA Entities)
- Exception handling should be reviewed and improved to support some error messages
- Use @Transaction annotation on services!

## 4. Is there any framework, library, tools that you know would greatly benefit this app and/or development ?

- There are 2 main alternatives frameworks now for spring I would consider: Quarkus and Micronaut. (Better GraalVM support at this time) 
  Of course there are plenty of options, like have it a serverless app (like AWS Lambda) and other. (And related tooling Serverless framework, terraform etc)
- Spring Cache ( @Cache ) - to cache reads  ( [Caffein](https://github.com/ben-manes/caffeine) for in memory or Redis for a real cache) to speed up performance
- Would use @Query annotation not implementing the `PizzaRepositoryImpl`.
- We could use QueryDSL for typesafe queries. (Supported by Spring Data JPA)
- AssertJ for nicer asserts

## 5. Others

- Note that code quality wise there could be more improvements, like use autowiring in constructors etc, 
  or values like `pizzas@gmail.com` should be probably configurable for test environments etc. 
  I did not articulate those in the sections above.
  - However, made some "todo"-s on some places also in code, to highlight some remarks from above.   
  



   